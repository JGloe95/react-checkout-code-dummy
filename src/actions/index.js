export const pushPersonData = (personalDataObject) => ({
  type: 'PERSONAL_DATA_PUSHED',
  payload: personalDataObject,
});

export const pushDeliverySelection = (deliverySelection) => ({
  type: 'DELIVERY_SELECTION_PUSHED',
  payload: deliverySelection,
});

export const pushPaymentSelection = (paymentSelection) => ({
  type: 'PAYMENT_SELECTION_PUSHED',
  payload: paymentSelection,
});

export const pushPersonDataAlternativeAddress = (
  // eslint-disable-next-line comma-dangle
  personalDataAlternativeAddressObject
) => ({
  type: 'PERSONAL_DATA_ALTERNATIVE_ADDRESS_PUSHED',
  payload: personalDataAlternativeAddressObject,
});

export const pushCheckoutCartPrice = (checkoutCartPrice) => ({
  type: 'CHECKOUT_CART_PRICE',
  payload: checkoutCartPrice,
});

export const resetCheckoutCartPrice = () => ({
  type: 'RESET_CART_PRICE',
});

export const redeemCheckoutVoucher = (checkoutCode) => ({
  type: 'REDEEM_CHECKOUT_VOUCHER',
  payload: checkoutCode,
});
