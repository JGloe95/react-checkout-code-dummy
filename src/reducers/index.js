import { combineReducers } from 'redux';

import deliverySelectionReducer from './deliverySelectionReducer';
import personalDataReducer from './personalDataReducer';
import personalDataAlternativeAddressReducer from './personalDataAlternativeAddressReducer';
import paymentSelectionReducer from './paymentSelectionReducer';
import productsReducer from './productsReducer';
import updateCartPriceReducer from './updateCartPriceReducer';

const rootReducer = combineReducers({
  personalData: personalDataReducer,
  personalDataAlternativeAddress: personalDataAlternativeAddressReducer,
  deliverySelection: deliverySelectionReducer,
  paymentSelection: paymentSelectionReducer,
  productsReducer,
  updateCartPriceReducer,
});

export default rootReducer;
