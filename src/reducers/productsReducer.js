const initialState = {
  product1: {
    image: '/images/jungen_jacke.jpg',
    name: 'Jungen Jacke',
    color: 'Rot',
    size: '98',
    number: '1',
    price: '12.99',
  },
  product2: {
    image: '/images/damen_hoodie.jpg',
    name: 'Damen Hoodie',
    color: 'Hellrot',
    size: '36/38',
    number: '1',
    price: '21.49',
  },
};

export default (state = initialState) => state;
