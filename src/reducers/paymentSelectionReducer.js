const initialState = {};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case 'PAYMENT_SELECTION_PUSHED':
      // Der vorherige State wird gelöscht, es wird nur Payload returned
      return payload;
    default:
      return state;
  }
};
