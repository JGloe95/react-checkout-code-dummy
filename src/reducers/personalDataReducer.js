const initialState = {};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case 'PERSONAL_DATA_PUSHED':
      // Der vorherige State wird gelöscht, es wird nur Payload returned
      return payload;
    default:
      return state;
  }
};
