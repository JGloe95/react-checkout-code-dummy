const initialState = 0;
let newState = 0;

export default (state = initialState, { type, payload }) => {
  switch (type) {
    // calculate cart-price (add every article-price)
    case 'CHECKOUT_CART_PRICE':
      newState = state + parseFloat(payload);
      return newState;
    case 'RESET_CART_PRICE':
      newState = 0;
      return newState;
    case 'REDEEM_CHECKOUT_VOUCHER':
      // if voucher is equal voucher10, then return price-state with 10% discount
      return payload === 'voucher10'
        ? parseFloat((state * 0.9).toFixed(2))
        : state;
    default:
      return state;
  }
};
