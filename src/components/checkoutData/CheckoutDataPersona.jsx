import React from 'react';
import { connect } from 'react-redux';

import InputFormGroup from '../formElements/InputFormGroup';
import RadioButton from '../formElements/RadioButton';

function CheckoutData({
  alternativeAddress,
  personalData,
  personalDataAlternativeAddress,
}) {
  let data = {};
  // Check if in step 1 "Ihre Daten" or in step 2 "Versand und Zahlart - alternative Adresse"
  if (alternativeAddress) {
    data = personalDataAlternativeAddress;
  } else {
    data = personalData;
  }

  return (
    <div>
      <div className="person-data card text-dark bg-light mb-3">
        <div className="card-header">Zu Ihrer Person</div>
        <div className="card-body">
          <div className="row">
            <legend className="col-form-label col-sm-2 pt-0">Anrede</legend>
            <div className="col-sm-10">
              <RadioButton
                labelName="Herr"
                idValueFor="male"
                groupName="askGender"
                isDefault={data.gender === 'male'}
              />
              <RadioButton
                labelName="Frau"
                idValueFor="female"
                groupName="askGender"
                isDefault={data.gender === 'female'}
              />
            </div>
          </div>
          <InputFormGroup
            forIdAttribute="firstName"
            typeAttribute="text"
            labelName="Vorname"
            value={data.firstName}
          />
          <InputFormGroup
            forIdAttribute="lastName"
            typeAttribute="text"
            labelName="Nachname"
            value={data.lastName}
          />
          <InputFormGroup
            forIdAttribute="birthDate"
            typeAttribute="date"
            labelName="Geboren am"
            value={data.birthDate}
          />
        </div>
      </div>
    </div>
  );
}

// Extract Data form Redux-Store to work with them as props
const mapStateToProps = (state) => ({
  personalData: state.personalData,
  personalDataAlternativeAddress: state.personalDataAlternativeAddress,
});

export default connect(mapStateToProps)(CheckoutData);
