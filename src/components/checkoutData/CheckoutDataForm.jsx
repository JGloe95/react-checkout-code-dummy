import React from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import {
  pushPersonData,
  pushPersonDataAlternativeAddress,
} from '../../actions';
import CheckoutDataPersona from './CheckoutDataPersona';
import CheckoutDataAddress from './CheckoutDataAddress';
import FormButton from '../formElements/FormButton';

function CheckoutDataForm({ alternativeAddress, buttonText }) {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // Return object with the input-value of each field in the Data-Form
  const getInputValue = () => {
    const personObject = {};

    const inputObjects = [
      'firstName',
      'lastName',
      'birthDate',
      'streetAndNumber',
      'addressComplement',
      'city',
      'zipCode',
      'mailAddress',
    ];
    for (let i = 0; i < inputObjects.length; i += 1) {
      const { value } = document.getElementById(inputObjects[i]);
      personObject[inputObjects[i]] = value;
    }

    const radioButtonObjects = ['male', 'female'];
    for (let i = 0; i < radioButtonObjects.length; i += 1) {
      if (document.getElementById(radioButtonObjects[i]).checked) {
        personObject.gender = radioButtonObjects[i];
      }
    }
    return personObject;
  };

  // Dispatch Object with input-values of the data-form to the reducer and navigate to next step
  const onButtonClick = (event) => {
    event.preventDefault();
    const inputValueArray = getInputValue();
    if (alternativeAddress) {
      dispatch(pushPersonDataAlternativeAddress(inputValueArray));
    } else {
      dispatch(pushPersonData(inputValueArray));
      navigate('/checkoutOptions');
    }
  };

  return (
    <form className="container">
      <h2>Ihre Daten</h2>
      <CheckoutDataPersona alternativeAddress={alternativeAddress} />
      <CheckoutDataAddress alternativeAddress={alternativeAddress} />
      <div className="button-container-submit-button">
        <FormButton
          buttonText={buttonText}
          onButtonClick={onButtonClick}
          buttonType="primary"
          dataTestId="submit-button"
        />
      </div>
    </form>
  );
}

export default CheckoutDataForm;
