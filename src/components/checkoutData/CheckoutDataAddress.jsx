import React from 'react';
import { connect } from 'react-redux';

import InputFormGroup from '../formElements/InputFormGroup';

function CheckoutDataAddress({
  alternativeAddress,
  personalData,
  personalDataAlternativeAddress,
}) {
  let data = {};
  // Check if in step 1 "Ihre Daten" or in step 2 "Versand und Zahlart - alternative Adresse"
  if (alternativeAddress) {
    data = personalDataAlternativeAddress;
  } else {
    data = personalData;
  }

  return (
    <div>
      <div className="person-data card text-dark bg-light mb-3">
        <div className="card-header">Ihre Adresse</div>
        <div className="card-body">
          <InputFormGroup
            forIdAttribute="streetAndNumber"
            typeAttribute="text"
            labelName="Straße und Hausnummer"
            value={data.streetAndNumber}
          />
          <InputFormGroup
            forIdAttribute="addressComplement"
            typeAttribute="text"
            labelName="Adresszusatz"
            value={data.addressComplement}
          />
          <InputFormGroup
            forIdAttribute="city"
            typeAttribute="text"
            labelName="Wohnort"
            value={data.city}
          />
          <InputFormGroup
            forIdAttribute="zipCode"
            typeAttribute="text"
            labelName="Postleitzahl"
            value={data.zipCode}
          />
          <InputFormGroup
            forIdAttribute="mailAddress"
            typeAttribute="email"
            labelName="E-Mail-Adresse"
            value={data.mailAddress}
          />
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  personalData: state.personalData,
  personalDataAlternativeAddress: state.personalDataAlternativeAddress,
});

export default connect(mapStateToProps)(CheckoutDataAddress);
