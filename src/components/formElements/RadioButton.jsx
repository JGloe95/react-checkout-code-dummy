import React from 'react';

function RadioButton({
  labelName,
  groupName,
  idValueFor,
  isDefault,
  onButtonClick,
}) {
  return (
    <fieldset className="form-group">
      <div className="form-check">
        <input
          className="form-check-input"
          type="radio"
          id={idValueFor}
          value={idValueFor}
          name={groupName}
          defaultChecked={isDefault ? 'checked' : ''}
          onClick={onButtonClick ? (event) => onButtonClick(event) : null}
          data-testid={idValueFor}
        />
        <label className="form-check-label" htmlFor={idValueFor}>
          {labelName}
        </label>
      </div>
    </fieldset>
  );
}

export default RadioButton;
