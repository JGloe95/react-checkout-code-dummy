import React from 'react';

function FormButton({
  buttonText, onButtonClick, buttonType, dataTestId,
}) {
  return (
    <div className="form-group row">
      <div className="col-sm-10">
        <button
          type="submit"
          className={`btn btn-${buttonType}`}
          onClick={(event) => onButtonClick(event)}
          data-testid={dataTestId}
        >
          {buttonText}
        </button>
      </div>
    </div>
  );
}

export default FormButton;
