import React from 'react';

function InputFormGroup({
  forIdAttribute,
  typeAttribute,
  labelName,
  placeholder,
  value,
}) {
  return (
    <div className="form-group">
      <label htmlFor={forIdAttribute}>{labelName}</label>
      <input
        type={typeAttribute}
        className="form-control"
        id={forIdAttribute}
        placeholder={placeholder}
        defaultValue={value}
        data-testid={forIdAttribute}
      />
    </div>
  );
}

export default InputFormGroup;
