import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { pushCheckoutCartPrice } from '../../actions';

function CheckoutCompletionArticleRow({
  image,
  name,
  color,
  size,
  number,
  price,
}) {
  const dispatch = useDispatch();

  const updateCartPrice = () => {
    dispatch(pushCheckoutCartPrice(price));
  };

  // rerender after avery state-reload
  useEffect(() => {
    updateCartPrice();
  }, []);

  return (
    <div>
      <div className="row product-article-row border">
        <div className="col-8 product-information-holder">
          <img src={image} className="rounded float-left" alt="product" />
          <div className="product-details-holder">
            <p className="article-name font-weight-bold">{name}</p>
            <p className="article-color">
              Farbe:
              {color}
            </p>
            <p className="size">
              Größe:
              {size}
            </p>
            <p className="number">
              Anzahl:
              {number}
            </p>
          </div>
        </div>
        <div className="col-4 product-price-holder">
          {price}
          {' '}
          €
        </div>
      </div>
    </div>
  );
}

export default CheckoutCompletionArticleRow;
