import React from 'react';
import { useDispatch } from 'react-redux';

import FormButton from '../formElements/FormButton';
import { redeemCheckoutVoucher } from '../../actions';

function CheckoutCompletionDiscountCard() {
  const dispatch = useDispatch();

  const onButtonClick = () => {
    dispatch(
      redeemCheckoutVoucher(document.querySelector('#voucherInputField').value),
    );
  };

  return (
    <div className="col discount-card card text-dark bg-light mb-3">
      <div className="card-header">Gutschein/Rabattcode einlösen</div>
      <div className="card-body">
        <input
          type="text"
          className="form-control"
          id="voucherInputField"
          placeholder="Ihr Gutschein-/Rabattcode"
          data-testid="voucher-input-field"
        />
        <FormButton
          buttonText="Einlösen"
          buttonType="dark"
          dataTestId="voucher-redeem-button"
          onButtonClick={onButtonClick}
        />
      </div>
    </div>
  );
}

export default CheckoutCompletionDiscountCard;
