import React from 'react';

function CheckoutCompletionPriceCard({ cartPrice }) {
  return (
    <div className="col cart-price-container">
      <div className="cart-price-information">
        <p className="price-label">Gesamtkosten inkl. MwSt.</p>
        <p data-testid="price-tag" className="price-tag">
          {cartPrice}
          {' '}
          €
        </p>
      </div>
    </div>
  );
}

export default CheckoutCompletionPriceCard;
