import React from 'react';

function CheckoutCompletionOverviewCard({
  cardTitle,
  cardText,
  onButtonClick,
}) {
  const checkGender = (personObject) => {
    if (personObject.gender === 'male') {
      return 'Herr';
    } if (personObject.gender === 'female') {
      return 'Frau';
    }
    return '';
  };

  const checkCardText = () => {
    // if cardText obejct, then it's personal-data-information
    if (typeof cardText === 'object') {
      return (
        <div data-testid="delivery-information-container">
          <p className="card-text-element">
            {checkGender(cardText)}
          </p>
          <p
            className="card-text-element"
            data-testid="first-name-information-container"
          >
            {cardText.firstName}
            {' '}
            {cardText.lastName}
          </p>
          <p className="card-text-element">{cardText.streetAndNumber}</p>
          <p className="card-text-element">
            {cardText.zipCode}
            {' '}
            {cardText.city}
          </p>
        </div>
      );
    }
    // otherwise it's the payment-information
    return cardText === 'invoice' ? 'Auf Rechnung' : 'Paypal';
  };

  return (
    <div>
      <div className="card checkout-completion-card">
        <div className="card-body">
          <h5 className="card-title">{cardTitle}</h5>
          <div className="card-text">{checkCardText()}</div>
          <button
            href="#"
            className="btn btn-dark change-information-button"
            onClick={onButtonClick}
            type="submit"
          >
            Ändern
          </button>
        </div>
      </div>
    </div>
  );
}

export default CheckoutCompletionOverviewCard;
