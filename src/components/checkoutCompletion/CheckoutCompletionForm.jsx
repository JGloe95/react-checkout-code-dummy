import React, { useLayoutEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';

import CheckoutCompletionOverviewCard from './CheckoutCompletionOverviewCard';
import FormButton from '../formElements/FormButton';
import CheckoutCompletionArticleRow from './CheckoutCompletionArticleRow';
import CheckoutCompletionDiscountCard from './CheckoutCompletionDiscountCard';
import CheckoutCompletionPriceCard from './CheckoutCompletionPriceCard';
import { resetCheckoutCartPrice } from '../../actions';

function CheckoutCompletionForm({
  paymentSelection,
  personalData,
  products,
  cartPrice,
}) {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const onDeliveryButtonClick = () => {
    navigate('/');
  };

  const onPaymentButtonClick = () => {
    navigate('/checkoutOptions');
  };

  const onSubmitButtonClick = () => {
    navigate('/checkoutCompletion');
  };

  const resetCartPrice = () => {
    dispatch(resetCheckoutCartPrice());
  };

  useLayoutEffect(() => {
    resetCartPrice();
  }, []);

  return (
    <div className="container">
      <h2>Prüfen & Bestellen</h2>
      <CheckoutCompletionOverviewCard
        // Aktuell kann nur Eingabe aus Hauptadresse angezeigt werden
        cardTitle="Versandadresse"
        cardText={personalData}
        onButtonClick={onDeliveryButtonClick}
      />
      <CheckoutCompletionOverviewCard
        cardTitle="Zahlungsart"
        cardText={paymentSelection}
        onButtonClick={onPaymentButtonClick}
      />
      <h2>Übersicht</h2>
      {/* Ggf. noch über state mappen um Wiederholung vermeiden */}
      <CheckoutCompletionArticleRow
        image={products.product1.image}
        name={products.product1.name}
        color={products.product1.color}
        size={products.product1.size}
        number={products.product1.number}
        price={products.product1.price}
      />
      <CheckoutCompletionArticleRow
        image={products.product2.image}
        name={products.product2.name}
        color={products.product2.color}
        size={products.product2.size}
        number={products.product2.number}
        price={products.product2.price}
      />
      <div className="row container calculate-price-container">
        <CheckoutCompletionDiscountCard />
        <CheckoutCompletionPriceCard cartPrice={cartPrice} />
      </div>
      <div className="button-container-submit-button">
        <FormButton
          buttonText="Jetzt kaufen"
          onButtonClick={onSubmitButtonClick}
          buttonType="success"
        />
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  paymentSelection: state.paymentSelection,
  personalData: state.personalData,
  products: state.productsReducer,
  cartPrice: state.updateCartPriceReducer,
});

export default connect(mapStateToProps)(CheckoutCompletionForm);
