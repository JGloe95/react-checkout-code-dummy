import React from 'react';
import { connect } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import '../App.scss';
import CheckoutOptionsDelivery from './CheckoutOptionsDelivery';
import CheckoutOptionsPayment from './CheckoutOptionsPayment';
import FormButton from '../formElements/FormButton';

function CheckoutOptionsForm({ personalData }) {
  const navigate = useNavigate();

  const onSubmitButtonClick = () => {
    navigate('/checkoutCompletion');
  };

  const onBackButtonClick = () => {
    navigate('/');
  };

  return (
    <div className="container checkout-options-container">
      <h2>Versand & Zahlart</h2>
      <div className="person-data card text-dark bg-light mb-3">
        <div className="card-header">Wohin sollen wir liefern?</div>
        <div className="card-body">
          <CheckoutOptionsDelivery personalData={personalData} />
        </div>
      </div>
      <div className="payment-method card text-dark bg-light mb-3">
        <div className="card-header">Wie möchten Sie bezahlen?</div>
        <div className="card-body">
          <CheckoutOptionsPayment />
        </div>
      </div>
      <div className="button-container-two-buttons">
        <FormButton
          buttonText="Zurück"
          onButtonClick={onBackButtonClick}
          buttonType="primary"
          dataTestId="back-button"
        />
        <FormButton
          buttonText="Weiter"
          onButtonClick={onSubmitButtonClick}
          buttonType="primary"
          dataTestId="submit-button"
        />
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({ personalData: state.personalData });

export default connect(mapStateToProps)(CheckoutOptionsForm);
