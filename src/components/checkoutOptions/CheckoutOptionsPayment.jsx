import React from 'react';
import { connect, useDispatch } from 'react-redux';

import RadioButton from '../formElements/RadioButton';
import { pushPaymentSelection } from '../../actions';

function CheckoutOptionsPayment({ paymentSelection }) {
  const dispatch = useDispatch();

  const onRadioButtonClick = (event) => {
    dispatch(pushPaymentSelection(event.target.value));
  };

  return (
    <div className="row">
      <legend className="col-form-label col-sm-2 pt-0">Zahlungsoption</legend>
      <div className="col-sm-10 payment-option-container">
        <RadioButton
          labelName="Paypal"
          idValueFor="paypal"
          groupName="askPaymentMethod"
          isDefault={paymentSelection === 'paypal'}
          onButtonClick={onRadioButtonClick}
        />
        <RadioButton
          labelName="Auf Rechnung"
          idValueFor="invoice"
          groupName="askPaymentMethod"
          isDefault={paymentSelection === 'invoice'}
          onButtonClick={onRadioButtonClick}
        />
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({ paymentSelection: state.paymentSelection });

export default connect(mapStateToProps)(CheckoutOptionsPayment);
