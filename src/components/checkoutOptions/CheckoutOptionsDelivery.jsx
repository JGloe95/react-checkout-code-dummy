import React from 'react';
import { connect, useDispatch } from 'react-redux';

import RadioButton from '../formElements/RadioButton';
import { pushDeliverySelection } from '../../actions';
import CheckoutDataForm from '../checkoutData/CheckoutDataForm';

function CheckoutOptionsDelivery({ personalData, deliverySelection }) {
  const dispatch = useDispatch();

  const onButtonClick = (event) => {
    dispatch(pushDeliverySelection(event.target.value));
  };

  const checkGender = (personObject) => {
    if (personObject.gender === 'male') {
      return 'Herr';
    } if (personObject.gender === 'female') {
      return 'Frau';
    }
    return '';
  };

  const renderDeliveryOptionDetails = () => {
    // Render important Data from Step 1 "Ihre Daten" within Step 2 "Versand und Zahlart"
    if (deliverySelection === 'homeAddress') {
      return (
        <div className="card" style={{ width: '18rem' }}>
          <div className="card-body">
            <div className="card-text" />
            <div className="personal-data" data-testid="personal-data-card">
              <p className="card-text-element">
                {checkGender(personalData)}
              </p>
              <p className="card-text-element">
                {personalData.firstName}
                {' '}
                {personalData.lastName}
              </p>
              <p className="card-text-element">
                {personalData.streetAndNumber}
              </p>
              <p className="card-text-element">
                {personalData.zipCode}
                {' '}
                {personalData.city}
              </p>
            </div>
          </div>
        </div>
      );
    }
    // Render Form if "An Alternative Adresse" is selected
    return (
      <CheckoutDataForm alternativeAddress buttonText="Speichern" />
    );
  };

  return (
    <div className="row">
      <legend className="col-form-label col-sm-2 pt-0">Lieferoption</legend>
      <div className="col-sm-10 delivery-option-container">
        <RadioButton
          labelName="An Rechnungsadresse"
          idValueFor="homeAddress"
          groupName="askDeliveryOption"
          isDefault={deliverySelection === 'homeAddress'}
          onButtonClick={onButtonClick}
        />
        <RadioButton
          labelName="An alternative Adresse"
          idValueFor="alternativeAddress"
          groupName="askDeliveryOption"
          isDefault={deliverySelection !== 'homeAddress'}
          onButtonClick={onButtonClick}
        />
        {renderDeliveryOptionDetails()}
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({ deliverySelection: state.deliverySelection });

export default connect(mapStateToProps)(CheckoutOptionsDelivery);
