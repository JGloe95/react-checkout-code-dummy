import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import Navigation from './Navigation';
import CheckoutDataForm from './checkoutData/CheckoutDataForm';
import CheckoutOptionsForm from './checkoutOptions/CheckoutOptionsForm';
import CheckoutCompletionForm from './checkoutCompletion/CheckoutCompletionForm';
import PageNotFound from './PageNotFound';

function App() {
  return (
    <Router>
      <Navigation />
      <Routes>
        <Route
          path="/"
          element={
            <CheckoutDataForm alternativeAddress={false} buttonText="Weiter" />
          }
        />
        <Route path="/checkoutOptions" element={<CheckoutOptionsForm />} />
        <Route
          path="/checkoutCompletion"
          element={<CheckoutCompletionForm />}
        />
        <Route path="*" element={<PageNotFound />} />
      </Routes>
    </Router>
  );
}

export default App;
