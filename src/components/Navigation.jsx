import React from 'react';
import { Link } from 'react-router-dom';

function Navigation() {
  return (
    <div className="container">
      <ul className="nav nav-justified border">
        <li className="nav-item">
          <div className="nav-link" aria-current="page">
            <Link to="/">Ihre Daten</Link>
          </div>
        </li>
        <li className="nav-item">
          <div className="nav-link">
            <Link to="/checkoutOptions">Versand und Zahlart</Link>
          </div>
        </li>
        <li className="nav-item">
          <div className="nav-link">
            <Link to="/checkoutCompletion">Bestätigung</Link>
          </div>
        </li>
      </ul>
    </div>
  );
}

export default Navigation;
