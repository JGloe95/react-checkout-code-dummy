import { cleanup } from '@testing-library/react';
import { render, screen, fireEvent } from '../../test-utils';
import '@testing-library/jest-dom/extend-expect';
import { BrowserRouter } from 'react-router-dom';

import CheckoutCompletionForm from '../checkoutCompletion/CheckoutCompletionForm';
import CheckoutCompletionOverviewCard from '../checkoutCompletion/CheckoutCompletionOverviewCard';
import updateCartPriceReducer from '../../reducers/updateCartPriceReducer';

const MockCheckoutCompletionForm = () => {
  return (
    <BrowserRouter>
      <CheckoutCompletionForm />
    </BrowserRouter>
  );
};

afterEach(() => {
  cleanup();
});

test('Should render address overview card', () => {
  const cardText = {
    gender: 'male',
    firstName: 'Jan',
    lastName: 'Gloe',
    streetAndNumber: 'Gantweger Str. 8',
    zipCode: '48727',
    city: 'Billerbeck',
  };
  render(
    <CheckoutCompletionOverviewCard
      cardTitle='Test'
      onButtonClick={null}
      cardText={cardText}
    />
  );

  const deliveryInformationContainer = screen.getByTestId(
    'delivery-information-container'
  );
  expect(deliveryInformationContainer).toBeInTheDocument;
  expect(deliveryInformationContainer).toHaveTextContent('Herr');
  expect(deliveryInformationContainer).toHaveTextContent('Jan');
  expect(deliveryInformationContainer).toHaveTextContent('Gloe');
  expect(deliveryInformationContainer).toHaveTextContent('Gantweger Str. 8');
  expect(deliveryInformationContainer).toHaveTextContent('48727');
  expect(deliveryInformationContainer).toHaveTextContent('Billerbeck');
});

test('Check voucher handling and new price calculation when rendered', () => {
  render(<MockCheckoutCompletionForm />);

  const priceTag = screen.getByTestId('price-tag');
  expect(priceTag).toBeInTheDocument;
  expect(priceTag).toHaveTextContent('34.48');

  const voucherInputField = screen.getByTestId('voucher-input-field');
  fireEvent.change(voucherInputField, { target: { value: 'voucher10' } });
  expect(voucherInputField.value).toBe('voucher10');
  const submitButton = screen.getByTestId('voucher-redeem-button');
  fireEvent.click(submitButton);

  expect(priceTag).toHaveTextContent('31.03');
});

test('Correct calculation of new cart value in reducer', () => {
  expect(
    updateCartPriceReducer(100, { type: 'CHECKOUT_CART_PRICE', payload: '-30' })
  ).toBe(70);
});

test('Correct calculation of voucher', () => {
  expect(
    updateCartPriceReducer(162.39, {
      type: 'REDEEM_CHECKOUT_VOUCHER',
      payload: 'voucher10',
    })
  ).toBe(146.15);
});

test('Should return 0 as initial state', () => {
  expect(updateCartPriceReducer(undefined, {})).toBe(0);
});
