import { cleanup } from '@testing-library/react';
import { render, screen, fireEvent } from '../../test-utils';
import '@testing-library/jest-dom/extend-expect';
import { BrowserRouter } from 'react-router-dom';

import CheckoutDataForm from '../checkoutData/CheckoutDataForm';
import CheckoutOptionsForm from '../checkoutOptions/CheckoutOptionsForm';

const MockCheckoutDataForm = () => {
  return (
    <BrowserRouter>
      <CheckoutDataForm />
    </BrowserRouter>
  );
};

const MockCheckoutOptionsForm = () => {
  return (
    <BrowserRouter>
      <CheckoutOptionsForm />
    </BrowserRouter>
  );
};

afterEach(() => {
  cleanup();
});

// Larger test-case than generally recommended
// But developed to check how big and reliable Unit-Tests with these tools can be
test('Should render first & last name input field, set input and store in state', () => {
  render(<MockCheckoutDataForm />);

  const firstNameInput = screen.getByTestId('firstName');
  fireEvent.change(firstNameInput, { target: { value: 'Max' } });
  expect(firstNameInput.value).toBe('Max');

  const lastNameInput = screen.getByTestId('lastName');
  fireEvent.change(lastNameInput, { target: { value: 'Mustermann' } });
  expect(lastNameInput.value).toBe('Mustermann');

  //save in store with button-click
  const submitButton = screen.getByTestId('submit-button');
  fireEvent.click(submitButton);

  render(<MockCheckoutOptionsForm />);

  const deliveryContainer = screen.findByText('Wohin sollen wir liefern?');
  expect(deliveryContainer).toBeInTheDocument;

  const backButton = screen.getByTestId('back-button');
  fireEvent.click(backButton);

  expect(firstNameInput.value).toBe('Max');
  expect(lastNameInput.value).toBe('Mustermann');
});

// Larger test-case than generally recommended
// But developed to check how big and reliable Unit-Tests with these tools can be
test('Should render Street, number & city input field, set input and store in state', () => {
  render(<MockCheckoutDataForm />);

  const firstNameInput = screen.getByTestId('streetAndNumber');
  fireEvent.change(firstNameInput, { target: { value: 'Industriestr. 1' } });
  expect(firstNameInput.value).toBe('Industriestr. 1');

  const lastNameInput = screen.getByTestId('city');
  fireEvent.change(lastNameInput, { target: { value: 'Coesfeld-Lette' } });
  expect(lastNameInput.value).toBe('Coesfeld-Lette');

  //save in store with button-click
  const submitButton = screen.getByTestId('submit-button');
  fireEvent.click(submitButton);

  render(<MockCheckoutOptionsForm />);

  const deliveryContainer = screen.findByText('Wohin sollen wir liefern?');
  expect(deliveryContainer).toBeInTheDocument;

  const backButton = screen.getByTestId('back-button');
  fireEvent.click(backButton);

  expect(firstNameInput.value).toBe('Industriestr. 1');
  expect(lastNameInput.value).toBe('Coesfeld-Lette');
});
