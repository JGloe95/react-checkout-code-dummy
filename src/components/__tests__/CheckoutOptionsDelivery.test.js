import { cleanup } from '@testing-library/react';
import { render, screen } from '../../test-utils';
import '@testing-library/jest-dom/extend-expect';

import CheckoutOptionsDelivery from '../checkoutOptions/CheckoutOptionsDelivery';
import CheckoutOptionsPayment from '../checkoutOptions/CheckoutOptionsPayment';

afterEach(() => {
  cleanup();
});

test('Should render personalData component and text', () => {
  const personalData = { firstName: 'Max', lastName: 'Mustermann' };
  render(
    <CheckoutOptionsDelivery
      personalData={personalData}
      deliverySelection='homeAddress'
    />
  );
  const personalDataCard = screen.getByTestId('personal-data-card');
  expect(personalDataCard).toBeInTheDocument;
  expect(personalDataCard).toHaveTextContent('Max', 'Mustermann');
});

test('Should render payment component and text', () => {
  const paymentSelection = 'paypal';
  render(<CheckoutOptionsPayment paymentSelection={paymentSelection} />);
  const paypalRadioButton = screen.getByTestId('paypal');
  expect(paypalRadioButton).toBeInTheDocument;
  const invoiceRadioButton = screen.getByTestId('invoice');
  expect(invoiceRadioButton).toBeInTheDocument;
});
