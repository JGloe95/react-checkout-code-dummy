# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## :scroll: Über dieses Projekt

Dieses Projekt bildet die Code-Grundlage für die Praxisarbeit im Rahmen des Master-Studiengangs "IT-Management" der WWU Münster.

## :rocket: Getting started

Mit den nachfolgenden Befehlen kann das Projekt aufgesetzt und genutzt werden.

### :desktop_computer: `npm install`

Nachdem das Projekt in die eigene lokale Umgebung geklont worden ist, 

### :racing_car: `npm start`

Führt den Dev-Mode aus und öffnet eine lokale Instanz "http://localhost:3000", welche im Browser geöffnet werden kann.
Die Seite lädt automatisch neu bei Änderungen.

### :eyes: `npm test`

Startet den Test-Modus und führt die zugehörigen Unit-Tests aus.


